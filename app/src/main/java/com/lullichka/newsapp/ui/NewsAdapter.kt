package com.lullichka.newsapp.ui

import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.databinding.BindingAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.lullichka.newsapp.databinding.NewsViewItemBinding
import com.lullichka.newsapp.model.News
import com.squareup.picasso.Picasso


class NewsAdapter : PagedListAdapter<News, NewsAdapter.NewsViewHolder>(NEWS_COMPARATOR) {
    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            (holder).bind(item)
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, DetailActivity::class.java)
            intent.putExtra(URL, item?.url)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = NewsViewItemBinding.inflate(layoutInflater, parent, false)
        return NewsViewHolder(itemBinding)
    }


    inner class NewsViewHolder(private val binding: NewsViewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: News) {
            binding.data = item
            binding.executePendingBindings()
        }
    }

    companion object {
        private val NEWS_COMPARATOR = object : DiffUtil.ItemCallback<News>() {
            override fun areItemsTheSame(oldItem: News, newItem: News): Boolean =
                oldItem.url == newItem.url

            override fun areContentsTheSame(oldItem: News, newItem: News): Boolean =
                oldItem == newItem
        }

        @JvmStatic
        @BindingAdapter("image")
        fun setImage(imageView: ImageView, url: String?) {
            if (!url.isNullOrEmpty()) {
                Picasso.get().load(url).centerCrop().fit().into(imageView)
            }
        }

        @JvmStatic
        @BindingAdapter("favColor")
        fun setFavColor(textView: TextView, favorite: Boolean) {
            if (favorite) {
                textView.setTextColor(textView.context.resources.getColor(com.lullichka.newsapp.R.color.colorAccent))
            } else textView.setTextColor(textView.context.resources.getColor(com.lullichka.newsapp.R.color.colorPrimaryDark))
        }
        private const val URL = "url"
    }

}



