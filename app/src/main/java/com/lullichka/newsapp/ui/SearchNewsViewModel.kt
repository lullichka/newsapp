package com.lullichka.newsapp.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import android.databinding.BindingAdapter
import android.widget.Spinner
import com.lullichka.newsapp.SpinnerExtensions.setSpinnerEntries
import com.lullichka.newsapp.data.NewsRepository
import com.lullichka.newsapp.model.News
import com.lullichka.newsapp.model.NewsSearchResult


class SearchNewsViewModel(private val repository: NewsRepository) : ViewModel() {

    private val queryLiveData = MutableLiveData<String>()
    private val sortLiveData = MutableLiveData<String>()
    private val fromLiveData = MutableLiveData<HashMap<String, String>>()
    val sortKeys = arrayListOf("popularity", "publishedAt")


    private var byDateNewsResult: LiveData<NewsSearchResult> =
        Transformations.map(fromLiveData) { repository.getByDate(it["from"]!!, it["to"]!!) }

    val byDateNews: LiveData<PagedList<News>> = Transformations.switchMap(
        byDateNewsResult
    ) { it -> it.data }

    private var sortedNewsResult: LiveData<NewsSearchResult> =
        Transformations.map(sortLiveData) { repository.getTopFromUsSorted(it) }

    val sortedNews: LiveData<PagedList<News>> = Transformations.switchMap(
        sortedNewsResult
    ) { it -> it.data }

    private var searchResult: LiveData<NewsSearchResult> = Transformations.map(queryLiveData) { repository.search(it) }

    val searchNews: LiveData<PagedList<News>> = Transformations.switchMap(
        searchResult
    ) { it -> it.data }

    val networkErrors: LiveData<String> = Transformations.switchMap(
        searchResult
    ) { it -> it.networkErrors }

    fun searchNews(queryString: String) {
        queryLiveData.postValue(queryString)
    }

    fun sortNews(sort: String) {
        sortLiveData.postValue(sort)
    }

    fun dateNews(dates: HashMap<String, String>) {
        fromLiveData.postValue(dates)
    }

    fun addToFavorites(item: News) {
        repository.updateFav(item.url, !item.favorite)
    }


    companion object {
        @JvmStatic
        @BindingAdapter("entries")
        fun Spinner.setEntries(entries: List<Any>?) {
            setSpinnerEntries(entries)
        }
    }
}