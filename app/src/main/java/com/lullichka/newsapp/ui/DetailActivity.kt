package com.lullichka.newsapp.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.lullichka.newsapp.BR
import com.lullichka.newsapp.Injection
import com.lullichka.newsapp.R
import com.lullichka.newsapp.databinding.ActivityDetailBinding
import com.lullichka.newsapp.model.News

class DetailActivity : AppCompatActivity() {

    private lateinit var viewModel: DetailViewModel
    private var newsUrl: String = ""
    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
            .get(DetailViewModel::class.java)
        binding.setVariable(BR.detailViewModel, viewModel)
        newsUrl = intent.getStringExtra(URL)
        if (!newsUrl.isNullOrEmpty()) {
            viewModel.queryNews(newsUrl)
        }
        viewModel.news?.observe(this, Observer<News> {
            binding.data = it
        })
    }

    companion object {
        private const val URL = "url"
    }
}
