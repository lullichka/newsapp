package com.lullichka.newsapp.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.lullichka.newsapp.data.NewsRepository
import com.lullichka.newsapp.model.News

/**
 * Created by Julia Alekseeva on 3/1/19.
 */
class DetailViewModel(val repository: NewsRepository) : ViewModel() {

    var news: LiveData<News>? = null

    fun queryNews(newsUrl: String?) {
        news = MutableLiveData<News>()
        if (!newsUrl.isNullOrEmpty()) {
            news = repository.getOneItem(newsUrl)
        }
    }
}