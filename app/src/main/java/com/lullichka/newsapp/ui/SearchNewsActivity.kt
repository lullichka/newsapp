package com.lullichka.newsapp.ui

import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Toast
import com.lullichka.newsapp.BR
import com.lullichka.newsapp.Injection
import com.lullichka.newsapp.R
import com.lullichka.newsapp.databinding.ActivityNewsBinding
import com.lullichka.newsapp.model.News
import kotlinx.android.synthetic.main.activity_news.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class SearchNewsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNewsBinding
    private lateinit var viewModel: SearchNewsViewModel
    private val adapter = NewsAdapter()
    var calendar: Calendar = Calendar.getInstance()
    val myFormat = "MM-dd-yy"
    val sdf = SimpleDateFormat(myFormat, Locale.US)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news)
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
            .get(SearchNewsViewModel::class.java)
        binding.setVariable(BR.searchViewModel, viewModel)
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        list.addItemDecoration(decoration)
        initAdapter()
        initSearch()
        initSpinner()
        initDatePickers()
    }

    private fun initDatePickers() {
        et_date_from.setOnClickListener {
            DatePickerDialog(
                it.context, dateFrom, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
        et_date_to.setOnClickListener {
            DatePickerDialog(
                it.context, dateTo, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    var dateFrom: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabelFrom()
        }
    var dateTo: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabelTo()
        }

    private fun updateLabelFrom() {
        et_date_from.setText(sdf.format(calendar.time))
        val dates = HashMap<String, String>()
        dates["from"] = sdf.format(calendar.time)
        dates["to"] = sdf.format(Calendar.getInstance().time)
        viewModel.dateNews(dates)
        adapter.submitList(null)
    }

    private fun updateLabelTo() {
        et_date_to.setText(sdf.format(calendar.time))
        val dates = HashMap<String, String>()
        dates["from"] = sdf.format(Calendar.getInstance().time)
        dates["to"] = sdf.format(calendar.time)
        viewModel.dateNews(dates)
        adapter.submitList(null)
    }

    private fun initAdapter() {
        list.adapter = adapter
        viewModel.searchNews.observe(this, Observer<PagedList<News>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.sortedNews.observe(this, Observer<PagedList<News>> {
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.byDateNews.observe(this, Observer<PagedList<News>> {
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this, "\uD83D\uDE28 Wooops ${it}", Toast.LENGTH_LONG).show()
        })
    }


    private fun initSearch() {
        search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updateListFromInput()
                true
            } else {
                false
            }
        }
        search.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updateListFromInput()
                true
            } else {
                false
            }
        }
    }

    private fun initSpinner() {
        spinner_sort_by.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position == 0) {
                    viewModel.sortNews("popularity")
                } else {
                    viewModel.sortNews("publishedAt")
                }
                adapter.submitList(null)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                viewModel.sortNews("popularity")
                adapter.submitList(null)
            }
        }
    }

    private fun updateListFromInput() {
        search.text.trim().let {
            if (it.isNotEmpty()) {
                list.scrollToPosition(0)
                viewModel.searchNews(it.toString())
                adapter.submitList(null)
            }
        }
    }


    private fun showEmptyList(show: Boolean) {
        if (show) {
            emptyList.visibility = View.VISIBLE
            list.visibility = View.GONE
        } else {
            emptyList.visibility = View.GONE
            list.visibility = View.VISIBLE
        }
    }
}
