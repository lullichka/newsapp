package com.lullichka.newsapp.data

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import com.lullichka.newsapp.api.NewsService
import com.lullichka.newsapp.api.getNewsSortedByDate
import com.lullichka.newsapp.api.getSortedNewsFromUs
import com.lullichka.newsapp.api.searchNews
import com.lullichka.newsapp.db.NewsLocalCache
import com.lullichka.newsapp.model.News

/**
 * Created by Julia Alekseeva on 3/2/19.
 */
class NewsBoundaryCallback(
    private val query: String,
    private val sortBy: String,
    private val from: String,
    private val to: String,
    private val type: Int,
    private val service: NewsService,
    private val cache: NewsLocalCache
) : PagedList.BoundaryCallback<News>() {
    private var isRequestInProgress = false
    private var lastRequestedPage = 1
    private val _networkErrors = MutableLiveData<String>()
    // LiveData of network errors.
    val networkErrors: LiveData<String>
        get() = _networkErrors

    override fun onZeroItemsLoaded() {
        requestAndSaveDataTopNews(query, sortBy, from, to, type)
    }

    override fun onItemAtEndLoaded(itemAtEnd: News) {
        requestAndSaveDataTopNews(query, sortBy, from, to, type)
    }


    private fun requestAndSaveDataTopNews(query: String, sortBy: String, from: String, to: String, type: Int) {
        if (isRequestInProgress) return
        isRequestInProgress = true
        when (type) {
            0 -> {
                getSortedNewsFromUs(service, sortBy, lastRequestedPage, NETWORK_PAGE_SIZE, { news ->
                    cache.insert(news) {
                        lastRequestedPage++
                        isRequestInProgress = false
                    }
                }, { error ->
                    _networkErrors.postValue(error)
                    isRequestInProgress = false
                })
            }
            1 -> {
                searchNews(service, query, lastRequestedPage, NETWORK_PAGE_SIZE, { news ->
                    cache.insert(news) {
                        lastRequestedPage++
                        isRequestInProgress = false
                    }
                }, { error ->
                    _networkErrors.postValue(error)
                    isRequestInProgress = false
                })
            }
            2 -> {
                getNewsSortedByDate(service, sortBy, lastRequestedPage, NETWORK_PAGE_SIZE, from, to,
                    { news ->
                        cache.insert(news) {
                            lastRequestedPage++
                            isRequestInProgress = false
                        }
                    }, { error ->
                        _networkErrors.postValue(error)
                        isRequestInProgress = false
                    })
            }
        }

    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 20

    }
}
