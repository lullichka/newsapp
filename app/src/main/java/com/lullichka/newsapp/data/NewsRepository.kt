package com.lullichka.newsapp.data

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.util.Log
import com.lullichka.newsapp.api.NewsService
import com.lullichka.newsapp.db.NewsLocalCache
import com.lullichka.newsapp.model.News
import com.lullichka.newsapp.model.NewsSearchResult


class NewsRepository(
    private val service: NewsService,
    private val cache: NewsLocalCache
) {

    fun getOneItem(url: String): LiveData<News> {
        return cache.oneItemByUrl(url)
    }

    fun search(query: String): NewsSearchResult {
        Log.d("NewsRepository", "New query: $query")
        val boundaryCallback = NewsBoundaryCallback(query, "", "", "", 1, service, cache)
        val networkErrors = boundaryCallback.networkErrors
        val dataSourceFactory = cache.newsByName(query)
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return NewsSearchResult(data, networkErrors)
    }

    fun getTopFromUsSorted(sorted: String): NewsSearchResult {
        val boundaryCallback = NewsBoundaryCallback("", sorted, "", "", 0, service, cache)
        val networkErrors = boundaryCallback.networkErrors
        val dataSourceFactory = cache.topNewsFromUs()
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return NewsSearchResult(data, networkErrors)
    }

    fun getByDate(from: String, to: String): NewsSearchResult {
        val boundaryCallback = NewsBoundaryCallback("", "popularity", from, to, 2, service, cache)
        val networkErrors = boundaryCallback.networkErrors
        val dataSourceFactory = cache.topNewsFromUs()
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return NewsSearchResult(data, networkErrors)
    }

    fun updateFav(url: String, fav: Boolean) {
        cache.updateFav(url, fav)
    }

    companion object {
        private const val DATABASE_PAGE_SIZE = 10

    }
}