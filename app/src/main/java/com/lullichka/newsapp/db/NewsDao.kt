package com.lullichka.newsapp.db

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.lullichka.newsapp.model.News


@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(news: List<News>)

    @Query(
        "SELECT * FROM news WHERE (title LIKE :queryString) OR (description LIKE " +
                ":queryString) ORDER BY author DESC"
    )
    fun newsByName(queryString: String): DataSource.Factory<Int, News>

    @Query("SELECT * FROM news")
    fun topNews(): DataSource.Factory<Int, News>


    @Query("SELECT * FROM news WHERE url = :url")
    fun itemByUrl(url: String): LiveData<News>

    @Query("UPDATE news SET favorite = :fav WHERE url = :url")
    fun updateFav(url: String, fav: Boolean): Int

}