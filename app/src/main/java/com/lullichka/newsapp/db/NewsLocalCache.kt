package com.lullichka.newsapp.db

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.util.Log
import com.lullichka.newsapp.model.News
import java.util.concurrent.Executor


class NewsLocalCache(
    private val newsDao: NewsDao,
    private val ioExecutor: Executor
) {

    fun insert(news: List<News>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("NewsLocalCache", "inserting ${news.size} news")
            newsDao.insert(news)
            insertFinished()
        }
    }

    fun updateFav(url: String, fav: Boolean) {
        ioExecutor.execute {
            newsDao.updateFav(url, fav)
        }
    }

    fun newsByName(name: String): DataSource.Factory<Int, News> {
        val query = "%${name.replace(' ', '%')}%"
        return newsDao.newsByName(query)
    }

    fun topNewsFromUs(): DataSource.Factory<Int, News> {
        return newsDao.topNews()
    }

    fun oneItemByUrl(url: String): LiveData<News> {
        return newsDao.itemByUrl(url)
    }
}