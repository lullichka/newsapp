package com.lullichka.newsapp.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by Julia Alekseeva on 3/1/19.
 */

@Entity(tableName = "news")
data class News(
    @PrimaryKey @field:SerializedName("url") val url: String,
    @field:SerializedName("author") val author: String?,
    @field:SerializedName("title") val title: String?,
    @field:SerializedName("description") val description: String?,
    @field:SerializedName("urlToImage") val urlToImage: String?,
    @field:SerializedName("publishedAt") val publishedAt: String?,
    @field:SerializedName("content") val content: String?,
    @field:SerializedName("favorite") val favorite: Boolean = false

)
