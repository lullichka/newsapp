package com.lullichka.newsapp.model

import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by Julia Alekseeva on 3/1/19.
 */

data class Source(
    @field:SerializedName("id") val id: Long?,
    @PrimaryKey @field:SerializedName("name") val name: String
)
