package com.lullichka.newsapp.api

import com.google.gson.annotations.SerializedName
import com.lullichka.newsapp.model.News

data class NewsSearchResponse(
    @SerializedName("totalResults") val total: Int = 0,
    @SerializedName("status") val status: String,
    @SerializedName("articles") val articles: List<News> = emptyList()
)
