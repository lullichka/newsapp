package com.lullichka.newsapp.api

import android.util.Log
import com.lullichka.newsapp.model.News
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Julia Alekseeva on 3/1/19.
 */

private const val TAG = "NewsService"
private const val API_KEY = "35b92ca9623842c1b5ddec887fbabc2f"

fun searchNews(
    service: NewsService,
    query: String,
    page: Int,
    pageSize: Int,
    onSuccess: (repos: List<News>) -> Unit,
    onError: (error: String) -> Unit
) {
    Log.d(TAG, "query: $query")
    val apiQuery = query
    service.searchNews(apiQuery, API_KEY, page, pageSize).enqueue(
        object : Callback<NewsSearchResponse> {
            override fun onFailure(call: Call<NewsSearchResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<NewsSearchResponse>?,
                response: Response<NewsSearchResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.articles ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

fun getSortedNewsFromUs(
    service: NewsService,
    sortBy: String,
    page: Int,
    pageSize: Int,
    onSuccess: (repos: List<News>) -> Unit,
    onError: (error: String) -> Unit
) {
    service.getSortedNewsFromUs("US", API_KEY, sortBy, page, pageSize).enqueue(
        object : Callback<NewsSearchResponse> {
            override fun onFailure(call: Call<NewsSearchResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<NewsSearchResponse>?,
                response: Response<NewsSearchResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.articles ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

fun getNewsSortedByDate(
    service: NewsService,
    sortBy: String,
    page: Int,
    pageSize: Int,
    from: String,
    to: String,
    onSuccess: (repos: List<News>) -> Unit,
    onError: (error: String) -> Unit
) {
    service.getNewsFromToDate("US", API_KEY, sortBy, page, pageSize, from, to).enqueue(
        object : Callback<NewsSearchResponse> {
            override fun onFailure(call: Call<NewsSearchResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<NewsSearchResponse>?,
                response: Response<NewsSearchResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.articles ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}


interface NewsService {

    @GET("everything")
    fun searchNews(
        @Query("q") query: String,
        @Query("apiKey") apiKey: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): Call<NewsSearchResponse>

    @GET("top-headlines")
    fun getSortedNewsFromUs(
        @Query("country") country: String,
        @Query("apiKey") apiKey: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): Call<NewsSearchResponse>

    @GET("top-headlines")
    fun getNewsFromToDate(
        @Query("country") country: String,
        @Query("apiKey") apiKey: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int,
        @Query("from") from: String,
        @Query("to") to: String
    ): Call<NewsSearchResponse>

    companion object {
        private const val BASE_URL = "https://newsapi.org/v2/"

        fun create(): NewsService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NewsService::class.java)
        }
    }
}
