package com.lullichka.newsapp

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.lullichka.newsapp.api.NewsService
import com.lullichka.newsapp.data.NewsRepository
import com.lullichka.newsapp.db.NewsDatabase
import com.lullichka.newsapp.db.NewsLocalCache
import com.lullichka.newsapp.ui.ViewModelFactory
import java.util.concurrent.Executors


object Injection {

    private fun provideCache(context: Context): NewsLocalCache {
        val database = NewsDatabase.getInstance(context)
        return NewsLocalCache(database.newsDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideNewsRepository(context: Context): NewsRepository {
        return NewsRepository(NewsService.create(), provideCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideNewsRepository(context))
    }

}